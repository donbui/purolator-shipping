var gulp = require('gulp')
var mjml = require('gulp-mjml')
var mjmlEngine = require('mjml')
var htmlmin = require('gulp-htmlmin');
var connect = require('gulp-connect')

gulp.task('mjml', function () {
  return gulp.src(['./templates/*.mjml'])
    // .pipe(mjml(mjmlEngine, {minify: true}))
		.pipe(mjml(mjmlEngine))
		// .pipe(htmlmin({ collapseWhitespace: false }))
    .pipe(gulp.dest('./dist'))
})

gulp.task('watch', function() {
	gulp.watch('./templates/*.mjml', ['mjml']);
	gulp.watch('./components/*.mjml', ['mjml']);
});

gulp.task('connect', function() {
  connect.server({
    root: 'dist',
    livereload: true
  })
});

gulp.task('default', ['mjml', 'watch', 'connect'])
gulp.task('compile', ['mjml'])
