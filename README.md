# Purolator Shipping Notification Emails

## File structure
`/components`: reusable components for emails, include using `<mj-include path="./components/COMPONENT_NAME" />`  
`/index.html`: index file showing all components  
`/templates`: .mjml template files made of components  
`/dist`: HTML files output, minified  
`/img`: image assets  


## Requirements
Built using MJML, gulp for task automation.

Clone the repo and run
```
npm install
```

To watch a single .mjml file:
```
mjml --watch index.mjml -o index.html
```

To watch templates/components folders for changes:
```
gulp
```

To batch process the entire `/templates` folder:
```
gulp compile
```

Docs: https://mjml.io/documentation/#mjml-raw
